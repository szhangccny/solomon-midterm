# Learning Session

# Option #1  - Wordpress With Database

For those that chose option #1:

### Goal:  
- Deploy a Wordpress Instance https://hub.docker.com/_/wordpress on Compute Engine or Cloud Run, given the requirments:
- Can use Docker Hub or Artifact Registry to store your image(s)
- Can use a Cloud SQL Database or database within your Compute Engine
- Proceed through the full Wordpress Setup to get to the "Hello World" theme
- Can use a top level domain or subdomain

# Option #2  - Hugo On Cloud Run

For those that chose option #1:

### Goal:  
- Deploy a Hugo Instance using Cloud Run https://gohugo.io/documentation/
- Must use Artifact Registry
- Max number of instances must be 1
- Max number of request per container must be 1000
- Region, generation, and other requirements not listed above are up to you.
- You can use any free Hugo theme https://hugothemesfree.com/, however your site must look like the demo - so recommend choosing a simple theme.
- Can use a top level domain or subdomain


#### Submission Instructions:
1. Name Your Project using the following paramters: "FirstName-LastName-City-Midterm"

2. Invite the professor to your GCP IAM Instance as am "editor".  Instructions are here is needed
https://www.howtogeek.com/devops/how-to-add-new-users-to-your-google-cloud-platform-projects/

3. Commit the project changes to your individual gitlab repos in a new branch called "Your FirstName-LastName-City-Midterm.


#### Some Helpful Notes:

##### You may or may not need to run some of the following commands:
- You can use anyonline resources or references to complete the midterm.
- Watch out for tutorials and guides that are older and may be inaccutrate.